Sample app:
- Login
- Show all member and show member of result search from db.users
- Information to login:
    Email: admin@mail.com
    Password: admin123

Structure of project:
    Sample-App:
    ---fn
    |---Assignment1.sql
    |---db.js (this is config db)
    |---users.csv (file data)
    ---middle-wares
    |---restrict.js
    ---models
    |---account.js
    ---routes
    |---accountRoute.js
    |---homeRoute.js
    ---views
    |---account
        |---login.hbs
    |---home
        |---index.hbs
    |---_layouts
        |---main.hbs
    ---public
    |---Image
    |---JS
    app.js
    package.json
    
Need to do in continue:
    - Navigation in member list page.
    - Ajax in search engine
    - Change some css when login wrong, route login after without
    