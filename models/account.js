var Q = require('q');
var mustache = require('mustache');

var db = require('../fn/db');

// exports.insert = function(entity) {
//
//     var deferred = Q.defer();
//
//     var sql =
//         mustache.render(
//             'insert into users (f_Email, f_Password, f_Name) values ("{{username}}", "{{password}}", "{{name}}", "{{email}}", "{{dob}}", {{permission}})',
//             entity
//         );
//
//     db.insert(sql).then(function(insertId) {
//         deferred.resolve(insertId);
//     });
//
//     return deferred.promise;
// }

exports.login = function(entity) {

    var deferred = Q.defer();

    var sql =
        mustache.render(
            'select * from users where f_Email = "{{username}}" and f_Password = "{{password}}"',
            entity
        );

    db.load(sql).then(function(rows) {
        if (rows.length > 0) {
            var user = {
                id: rows[0].f_ID,
                username: rows[0].f_Username,
                name: rows[0].f_Name,
                email: rows[0].f_Email,
                dob: rows[0].f_DOB,
                permission: rows[0].f_Permission
            }
            deferred.resolve(user);
        } else {
            deferred.resolve(null);
        }
    });

    return deferred.promise;
}

exports.access = function() {
  var deferred = Q.defer();

  var sql = 'SELECT * from users';

  db.load(sql).then(function(rows) {
      deferred.resolve(rows);
  });

  return deferred.promise;
}

exports.returnQuery = function(key) {
  var deferred = Q.defer();

  var sql = 'SELECT * from users  \
              where (f_Name like "%' + key + '%") or (f_Interest like "%' + key + '%") \
                    or (f_Region like "%' + key + '%") or (f_Specialism like "%' + key + '%")';
  db.load(sql).then(function(rows) {
      deferred.resolve(rows);
  });

  return deferred.promise;
}
