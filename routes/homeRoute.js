var express = require('express');
var restrict = require('../middle-wares/restrict');

var users = require('../models/account.js')
var retQuery = require('../models/account.js')
var db = require('../fn/db')

var homeRoute = express.Router();

homeRoute.get('/', restrict, function(req, res) {
  users.access().then(function(data)
  {
    // console.log(data[0]);
    res.render('home/index', {
        layoutModels: res.locals.layoutModels,
        users: data,
    });
  });
});

homeRoute.get('/search', function(req,res){
  retQuery.returnQuery(req.query.rawSearch).then(function(rows) {
// db.query('SELECT * from products where ProName like "%'+req.query.key+'%"', function(err, rows, fields) {
    // console.log("search = ?");
    console.log(rows);

	  // if (err) throw err;
    // var data = [];
    // for(i=0; i < rows.length; i++) {
    //   data.push(rows[i].ProName);
    // }
    //
    // res.end(JSON.stringify(data));
    // res.end('nothing');
    if (rows) {
        // redirect('home/index');
        res.render('home/index', {
            layoutModels: res.locals.layoutModels,
            retQuery: rows
        });
    }
	});
});

module.exports = homeRoute;
