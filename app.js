var path = require('path');

var express = require('express');
var session = require('express-session');

var MySQLStore = require('express-mysql-session')(session);

var exphbs = require('express-handlebars');
var exphbs_sections = require('express-handlebars-sections');

var bodyParser = require('body-parser');

var morgan = require('morgan');
var mustache = require('mustache');
var moment = require('moment');
var wnumb = require('wnumb');

// var layoutRoute = require('./routes/_layoutRoute');
var homeRoute = require('./routes/homeRoute');
var accountRoute = require('./routes/accountRoute');

var app = express();

// logger
app.use(morgan('short'));

// view engine
app.engine('hbs', exphbs({
  extname: 'hbs',
  defaultLayout: 'main',
  layoutsDir: 'views/_layouts/',
  partialsDir: 'views/_partials/',
  helpers: {
      section: exphbs_sections()
  }
}));

app.set('view engine', 'hbs');

// static files & favicon
app.use(express.static(
  path.resolve(__dirname, 'public')
));

// body-parser
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
  extended: false
}));

// session
app.use(session({
  secret: 'thisismyfirstappnodejsmysql',
  resave: false,
  saveUninitialized: true,
  store: new MySQLStore({
    host: '127.0.0.1',
    port: 3306,
    user: 'root',
    password: '@Dmin123',
    database: 'Assignment1',
    createDatabaseTable: true,
    schema: {
      tableName: 'sessions',
      columnNames: {
        session_id: 'session_id',
        expires: 'expires',
        data: 'data'
      }
    }
  }),
}));

// routes
app.use('/', homeRoute);
// app.use('/search', homeRoute)
app.use('/account', accountRoute);
app.use(function(req, res) {
  res.statusCode = 404;
  res.end('404 - FILE NOT FOUND!');
});

// start http-server
app.listen(3000);
